<?php

/**
*  to enable routing
*/
class Router
{
	
	protected static $tableOfRoutes = [];
	protected static $currentRoute = [];

	/**
	 * <b>to fill in the tableOfRoutes array</b>
	 * 
	 * @param string $regexp
	 * @param array $route
	 */
	public static function addRoute($regexp, $route = [])
	{

		self::$tableOfRoutes[$regexp] = $route;
	}

	/**
	 * <b>to return the table of routes</b>
	 * @return array
	 */
	public static function getTableOfRoutes()
	{

		return self::$tableOfRoutes;
	}

	/**
	 * <b>to return current root</b>
	 * @return string
	 */
	public static function getCurrentRoute()
	{

		return self::$currentRoute;
	}

	/**
	 * <b>to check if there're matches between requested url and registered routes
	 * @param string $url
	 * @return bool
	 */
	public static function checkRouteForMatches($url)
		{

		foreach (self::$tableOfRoutes as $pattern => $route) {
			
			if ($url == $pattern){

				self::$currentRoute = $route;
				return true;
			}
		}

		return false; //if there're no matches
	}





}