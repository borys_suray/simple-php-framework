<?php

/**
 *  <p>to display array $arr pretty within the <pre></pre> tag (debug function)</p>
 *  @param array $arr
 *	@return bool
 */
function displayArray($arr)
{

	if (!is_array($arr))
	{
		return false;
	}

	echo "<pre>";
	print_r($arr);
	echo "</pre>";
	
	return true;
}