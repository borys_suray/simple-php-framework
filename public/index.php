<?php

require '../vendor/core/Router.php';
require '../vendor/libs/functions.php';

echo $query = rtrim($_SERVER['QUERY_STRING'], '/');


$tableOfRoutes = require('../app/config/routes.php');


foreach ($tableOfRoutes as $regexp => $route) {
	
	Router::addRoute($regexp, $route);
}


displayArray(Router::getTableOfRoutes());


if (Router::checkRouteForMatches($query)){

	displayArray(Router::getCurrentRoute());

} else {

	echo '404';
}
